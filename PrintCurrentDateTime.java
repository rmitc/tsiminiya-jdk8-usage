import java.util.Date;

public class PrintCurrentDateTime {
    public static void main(String[] args) {
        System.out.println("\n\nDate-Time Now: ");
        System.out.println(new Date());
    }
}
